﻿using Photovoir.Interfaces.Dao.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.DataAccessObjects.Order
{
    public class OrderDetailsDao : IOrderDetailsDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[OrderDetails] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[OrderDetails]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[OrderDetails]
                   WHERE [Id] = @Id";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[OrderDetails] ([UserId], [PaymentId], [Total]
                    VALUES (@UserId, @PaymentId, @Total);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[OrderDetails] SET
                    [PaymentId] = @PaymentId,
                    [Total] = @Total,
                    WHERE [Id] = @Id";
        }
    }
}
