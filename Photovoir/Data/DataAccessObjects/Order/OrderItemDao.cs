﻿using Photovoir.Interfaces.Dao.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.DataAccessObjects.Order
{
    public class OrderItemDao : IOrderItemDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[OrderItem] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[OrderItem]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[OrderItem]
                   WHERE [Id] = @Id";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[OrderItem] ([OrderId], [ProductId]
                    VALUES (@OrderId, @PaymentId, @ProductId);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[OrderItem] SET
                    [ProductId] = @ProductId,
                    WHERE [Id] = @Id";
        }
    }
}
