﻿using Photovoir.Interfaces.Dao.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.DataAccessObjects
{
    public class CartItemDao : ICartItemDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[CartItem] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[CartItem]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[CartItem]
                   WHERE [Id] = @Id";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[CartItem] ([ShoppingCartId], [ProductId], [Quantity]
                    VALUES (@ShoppingCartId, @ProductId, @Quantity);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[CartItem] SET
                    [ProductId] = @ProductId,
                    [Quantity] = @Quantity,
                    WHERE [Id] = @Id";
        }
    }
}
