﻿using Photovoir.Interfaces.Dao.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.DataAccessObjects
{
    public class ShoppingCartDao : IShoppingCartDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[ShoppingCart] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[ShoppingCart]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[ShoppingCart]
                   WHERE [Id] = @Id";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[ShoppingCart] 
                    ([UserId], [Total])
                    VALUES (@UserId, @Total);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[OrderDetails] SET
                    [Total] = @Total,
                    WHERE [Id] = @Id";
        }
    }
}
