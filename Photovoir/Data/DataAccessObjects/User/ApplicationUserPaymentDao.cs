﻿using Photovoir.Interfaces.Dao.User;

namespace Photovoir.Data.DataAccessObjects
{
    public class ApplicationUserPaymentDao : IApplicationUserPaymentDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[ApplicationUserPayment] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[ApplicationUserPayment]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[ApplicationUserPayment]
                   WHERE [Id] = @Id";
        }

        public string GetByUserIdSql()
        {
            return @"SELECT * FROM [dbo].[ApplicationUserPayment]
                   WHERE [UserId] = @UserId";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[ApplicationUserPayment] ([UserId], [CardNumber], [NameOnCard], [Expiry])
                    VALUES (@UserId, @CardNumber, @NameOnCard, @Expiry);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[ApplicationUserPayment] SET
                    [CardNumber] = @CardNumber,
                    [NameOnCard] = @NameOnCard,
                    [Expiry] = @Expiry,
                    WHERE [Id] = @Id";
        }
    }
}
