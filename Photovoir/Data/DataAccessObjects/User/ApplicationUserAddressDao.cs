﻿using Photovoir.Interfaces.Dao.User;

namespace Photovoir.Data.DataAccessObjects
{
    public class ApplicationUserAddressDao : IApplicationUserAddressDao
    {
        public string DeleteSql()
        {
            return @"DELETE FROM [dbo].[ApplicationUserAddress] 
                   WHERE Id = @Id";
        }

        public string GetAllSql()
        {
            return @"Select * From [dbo].[ApplicationUserAddress]";
        }

        public string GetByIdSql()
        {
            return @"SELECT * FROM [dbo].[ApplicationUserAddress]
                   WHERE [Id] = @Id";
        }

        public string GetByUserIdSql()
        {
            return @"SELECT * FROM [dbo].[ApplicationUserAddress]
                   WHERE [UserId] = @UserId";
        }

        public string InsertSql()
        {
            return @"INSERT INTO [dbo].[ApplicationUserAddress] ([UserId], [AddressLine1], [AddressLine2],
                    [City], [PostalCode], [Country])
                    VALUES (@UserId, @AddressLine1, @AddressLine2, @City, @PostalCode, @Country);
                    SELECT SCOPE_IDENTITY()";
        }

        public string UpdateSql()
        {
            return @"UPDATE [dbo].[ApplicationUserAddress] SET
                    [AddressLine1] = @AddressLine1,
                    [AddressLine2] = @AddressLine2,
                    [City] = @City,
                    [PostalCode] = @PostalCode,
                    [Country] = @Country,
                    WHERE [Id] = @Id";
        }
    }
}
