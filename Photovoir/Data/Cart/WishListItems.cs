﻿using Photovoir.Data.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.Cart
{
    public class WishListItems
    {
        public int Id { get; set; }
        public int WishListId { get; set; }
        public Product Item { get; set; }
    }
}
