﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.User
{
    public class ApplicationUserPayment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CardNumber { get; set; }
        public string NameOnCard { get; set; }
        public string Expiry { get; set; }
    }
}
