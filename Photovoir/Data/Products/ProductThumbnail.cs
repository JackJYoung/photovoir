﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Data.Products
{
    public class ProductThumbnail
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public byte[] ByteData { get; set; }
    }
}
