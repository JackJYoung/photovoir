using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Photovoir.Data;
using Photovoir.Data.DataAccessObjects;
using Photovoir.Data.DataAccessObjects.Order;
using Photovoir.Data.User;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.Cart;
using Photovoir.Interfaces.Dao.Order;
using Photovoir.Interfaces.Dao.Product;
using Photovoir.Interfaces.Dao.User;
using Photovoir.Interfaces.Repositories.Cart;
using Photovoir.Interfaces.Repositories.Order;
using Photovoir.Interfaces.Repositories.Products;
using Photovoir.Interfaces.Repositories.User;
using Photovoir.Services;
using Photovoir.Services.Persistence.Helper;
using Photovoir.Services.Persistence.Repositories.Cart;
using Photovoir.Services.Persistence.Repositories.Orders;
using Photovoir.Services.Persistence.Repositories.Products;
using Photovoir.Services.Persistence.Repositories.User;
using Photovoir.Services.Stores;
using System.Data.Common;
using System.Data.SqlClient;

namespace Photovoir
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            // User + Role Stores
            services.AddTransient<IUserStore<ApplicationUser>, UserStore>();
            services.AddTransient<IUserEmailStore<ApplicationUser>, UserStore>();
            services.AddTransient<IUserPhoneNumberStore<ApplicationUser>, UserStore>();
            services.AddTransient<IUserPasswordStore<ApplicationUser>, UserStore>();
            services.AddTransient<IUserRoleStore<ApplicationUser>, UserStore>();
            services.AddTransient<IRoleStore<ApplicationRole>, RoleStore>();

            // Data Access Objects
            services.AddTransient<IProductDao, ProductDao>();
            services.AddTransient<ICartItemDao, CartItemDao>();
            services.AddTransient<IShoppingCartDao, ShoppingCartDao>();
            services.AddTransient<IApplicationUserAddressDao, ApplicationUserAddressDao>();
            services.AddTransient<IApplicationUserPaymentDao, ApplicationUserPaymentDao>();
            services.AddTransient<IOrderDetailsDao, OrderDetailsDao>();
            services.AddTransient<IOrderItemDao, OrderItemDao>();

            // Repositories
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ICartItemRepository, CartItemRepository>();
            services.AddTransient<IShoppingCartRepository, ShoppingCartRepository>();
            services.AddTransient<IApplicationUserAddressRepository, ApplicationUserAddressRepository>();
            services.AddTransient<IApplicationUserPaymentRepository, ApplicationUserPaymentRepository>();
            services.AddTransient<IOrderDetailsRepository, OrderDetailsRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();

            // Helpers
            services.AddTransient<ISqlHelper, SqlHelper>();
            services.AddTransient<ITransaction, Transaction>();

            // Identity
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddDefaultTokenProviders();

            // Services
            services.AddTransient<IEmailSender, EmailSender>();

            // ** Register this once at startup
            DbProviderFactories.RegisterFactory("Microsoft.Data.SqlClient", SqlClientFactory.Instance);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
