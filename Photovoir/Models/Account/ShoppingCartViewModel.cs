﻿using Photovoir.Data.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Models.Account
{
    public class ShoppingCartViewModel
    {
        public List<CartItem> CartItems { get; set; }
    }
}
