﻿using Photovoir.Data.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Models.Home
{
    public class FeaturedPanelViewModel
    {
        public Product[] FeaturedPhotography { get; set; }
    }
}
