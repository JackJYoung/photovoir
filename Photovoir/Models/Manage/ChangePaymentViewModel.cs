﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Models.Manage
{
    public class ChangePaymentViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "CardNumber")]
        public string CardNumber { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "NameOnCard")]
        public string NameOnCard { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "ExpiryDate")]
        public string ExpiryDate { get; set; }
    }
}
