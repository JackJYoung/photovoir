﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Models.Manage
{
    public class ChangeAddressViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "AddressLine2")]
        public string AddressLine2 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "City")]
        public string City { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "ZipCode")]
        public string ZipCode { get; set; }
    }
}
