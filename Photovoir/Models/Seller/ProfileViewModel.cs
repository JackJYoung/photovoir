﻿using Photovoir.Data.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Models.Seller
{
    public class ProfileViewModel
    {
        public List<Product> UserPosts { get; set; }
    }
}
