﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Dao
{
    public interface IDao
    {
        string InsertSql();
        string DeleteSql();
        string GetAllSql();
        string GetByIdSql();
        string UpdateSql();
    }
}
