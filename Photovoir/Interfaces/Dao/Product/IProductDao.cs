﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Dao.Product
{
    public interface IProductDao : IDao
    {
        string UpdateNameSql();
        string UpdatePriceSql();
        string UpdateTagsSql();
        string UpdateDescriptionSql();
        string GetByUserIdSql();
    }
}
