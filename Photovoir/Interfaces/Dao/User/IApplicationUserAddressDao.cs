﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Dao.User
{
    public interface IApplicationUserAddressDao : IDao
    {
        string GetByUserIdSql();
    }
}
