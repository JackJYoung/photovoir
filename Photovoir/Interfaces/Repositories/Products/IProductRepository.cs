﻿using Photovoir.Data.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Repositories.Products
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<List<Product>> GetByUserIdAsync(int UserId);
        Task<bool> UpdateName(int Id, string Name);
        Task<bool> UpdatePrice(int Id, string Price);
        Task<bool> UpdateTags(int Id, string Tags);
        Task<bool> UpdateDescription(int Id, string Description);
    }
}
