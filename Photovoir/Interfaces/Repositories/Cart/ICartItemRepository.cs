﻿using Photovoir.Data.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Repositories.Cart
{
    public interface ICartItemRepository : IRepository<CartItem>
    {
    }
}
