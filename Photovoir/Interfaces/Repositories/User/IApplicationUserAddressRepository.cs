﻿using Photovoir.Data.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Repositories.User
{
    public interface IApplicationUserAddressRepository : IRepository<ApplicationUserAddress>
    {
        Task<ApplicationUserAddress> GetByUserIdAsync(int UserId);
    }
}