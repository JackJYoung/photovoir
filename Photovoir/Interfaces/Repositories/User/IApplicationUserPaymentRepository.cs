﻿using Photovoir.Data.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Repositories.User
{
    public interface IApplicationUserPaymentRepository : IRepository<ApplicationUserPayment>
    {
        Task<ApplicationUserPayment> GetByUserIdAsync(int UserId);
    }
}
