﻿using Photovoir.Data.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Interfaces.Repositories.Order
{
    public interface IOrderItemRepository : IRepository<OrderItem>
    {
    }
}
