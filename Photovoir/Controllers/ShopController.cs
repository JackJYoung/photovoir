﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Photovoir.Data.Products;
using Photovoir.Interfaces.Repositories.Products;
using Photovoir.Models.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ShopController : Controller
    {
        private readonly ILogger<ShopController> _logger;
        private readonly IProductRepository _productRepository;

        public ShopController(ILogger<ShopController> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }
        public async Task<IActionResult> Index()
        {
            var model = new ShopViewModel
            {
                Products = new List<Product>()
            };

            var result = await _productRepository.GetAllAsync();
            foreach(Product p in result)
            {
                model.Products.Add(p);
            }

            return View(model);
        }

        public IActionResult ProductDetails()
        {
            return View();
        }
    }
}
