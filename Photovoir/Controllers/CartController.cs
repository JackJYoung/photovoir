﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class CartController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult BillingAddress()
        {
            return View();
        }
        public IActionResult PaymentMethod()
        {
            return View();
        }
        public IActionResult Checkout()
        {
            return View();
        }
    }
}
