﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Photovoir.Data.User;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Repositories.User;
using Photovoir.Models.Manage;

namespace Photovoir.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IUserStore<ApplicationUser> _userStore;
        private readonly IUserEmailStore<ApplicationUser> _emailStore;
        private readonly IUserPhoneNumberStore<ApplicationUser> _phoneStore;
        private readonly IApplicationUserAddressRepository _addressRepository;
        private readonly IApplicationUserPaymentRepository _paymentRepository;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        private const string AuthenicatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";

        public ManageController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IUserStore<ApplicationUser> userStore,
          IUserEmailStore<ApplicationUser> emailStore,
          IUserPhoneNumberStore<ApplicationUser> phoneStore,
          IApplicationUserAddressRepository addressRepository,
          IApplicationUserPaymentRepository paymentRepository,
          IEmailSender emailSender,
          ILogger<ManageController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userStore = userStore;
            _emailStore = emailStore;
            _phoneStore = phoneStore;
            _addressRepository = addressRepository;
            _paymentRepository = paymentRepository;
            _emailSender = emailSender;
            _logger = logger;
        }


        [HttpGet]
        public IActionResult Index(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            if (model.Username != null && model.Username != "")
                user.UserName = model.Username;

            if (model.Email != null && model.Email != "")
                user.Email = model.Email;

            if (model.PhoneNumber != null && model.PhoneNumber != "")
                user.PhoneNumber = model.PhoneNumber;

            var changePersonalDetailsResult = await _userManager.UpdateAsync(user);
            if (!changePersonalDetailsResult.Succeeded)
            {
                AddErrors(changePersonalDetailsResult);
                return View(model);
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            _logger.LogInformation("User updated successfully.");

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> ChangeAddress()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            var address = await _addressRepository.GetByUserIdAsync(user.Id);
            var model = new ChangeAddressViewModel();
            if(address != null)
            {
                model = new ChangeAddressViewModel
                {
                    AddressLine1 = address.AddressLine1,
                    AddressLine2 = address.AddressLine2,
                    City = address.City,
                    Country = address.Country,
                    ZipCode = address.PostalCode,
                };
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeAddress(ChangeAddressViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var address = await _addressRepository.GetByUserIdAsync(user.Id);
            if (address == null)
            {
                address = new ApplicationUserAddress
                {
                    UserId = user.Id,
                    AddressLine1 = model.AddressLine1,
                    AddressLine2 = model.AddressLine2,
                    City = model.City,
                    PostalCode = model.ZipCode,
                    Country = model.Country
                };
                await _addressRepository.AddAsync(address);
            }
            else
            {
                if (model.AddressLine1 != null)
                    address.AddressLine1 = model.AddressLine1;
                if (model.AddressLine2 != null)
                    address.AddressLine2 = model.AddressLine2;
                if (model.City != null)
                    address.City = model.City;
                if (model.ZipCode != null)
                    address.PostalCode = model.ZipCode;
                if (model.Country != null)
                    address.Country = model.Country;

                await _addressRepository.UpdateAsync(address);
            }

            return RedirectToAction(nameof(ChangeAddress));
        }

        [HttpGet]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new ChangePasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                AddErrors(changePasswordResult);
                return View(model);
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            _logger.LogInformation("User changed their password successfully.");

            return RedirectToAction(nameof(ChangePassword));
        }

        [HttpGet]
        public async Task<IActionResult> ChangePayment()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            var card = await _paymentRepository.GetByUserIdAsync(user.Id);
            var model = new ChangePaymentViewModel();
            if (card != null)
            {
                model = new ChangePaymentViewModel
                {
                    CardNumber = card.CardNumber,
                    NameOnCard = card.NameOnCard,
                    ExpiryDate = card.Expiry
                };
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePayment(ChangePaymentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var card = await _paymentRepository.GetByUserIdAsync(user.Id);
            if (card == null)
            {
                card = new ApplicationUserPayment
                {
                    UserId = user.Id,
                    CardNumber = model.CardNumber,
                    NameOnCard = model.NameOnCard,
                    Expiry = model.ExpiryDate,
                };
                await _paymentRepository.AddAsync(card);
            }
            else
            {
                await _paymentRepository.UpdateAsync(card);
            }

            return RedirectToAction(nameof(ChangePayment));
        }

        public IActionResult Orders()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
        #endregion
    }
}