﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Photovoir.Data.Products;
using Photovoir.Data.User;
using Photovoir.Interfaces.Repositories.Products;
using Photovoir.Models.Seller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Controllers
{
    public class SellerController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        public SellerController(IProductRepository productRepository, 
            UserManager<ApplicationUser> userManager)
        {
            _productRepository = productRepository;
            _userManager = userManager;
        }
        public async Task<IActionResult> Profile()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }
            var post = await _productRepository.GetByUserIdAsync(user.Id);
            var model = new ProfileViewModel
            {
                UserPosts = post
            };
            return View(model);
        }

        public IActionResult Products()
        {
            return View();
        }

        public IActionResult NewProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewProduct(NewProductViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var product = new Product
            {
                AuthorId = user.Id,
                Name = model.Name,
                Price = Convert.ToDouble(model.Price),
                Description = model.Description,
                Tags = model.Tags
            };

            await _productRepository.AddAsync(product);

            return RedirectToAction(nameof(Profile));
        }
    }
}
