﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Photovoir.Interfaces.Repositories.Products;
using Photovoir.Models;
using Photovoir.Models.Home;
using System.Diagnostics;
using Photovoir.Data.Products;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Photovoir.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProductRepository _productRepository;

        public HomeController(ILogger<HomeController> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }

        public async Task<IActionResult> Index()
        {
            var model = new FeaturedPanelViewModel
            {
                FeaturedPhotography = new Product[6]
            };

            for(int i = 0; i < model.FeaturedPhotography.Length; i++)
            {
                model.FeaturedPhotography[i] = await _productRepository.GetByIdAsync(i + 1);
            }
            return View(model);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult ContactUs()
        {
            return View();
        }

        public IActionResult FAQ()
        {
            return View();
        }

        [Authorize]
        public IActionResult ProductDetails()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}