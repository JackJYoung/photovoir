﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Photovoir.Data;
using Photovoir.Data.User;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.User;
using Photovoir.Interfaces.Repositories.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Services.Persistence.Repositories.User
{
    public class ApplicationUserAddressRepository : IApplicationUserAddressRepository
    {
        private readonly ISqlHelper _sqlHelper;
        private readonly ILogger<ApplicationUserAddressRepository> _logger;
        private readonly IApplicationUserAddressDao _dao;
        private readonly IConfiguration _config;

        public ApplicationUserAddressRepository(IConfiguration configuration, IApplicationUserAddressDao dao, ILogger<ApplicationUserAddressRepository> logger, ISqlHelper sqlHelper)
        {
            _config = configuration;
            _sqlHelper = sqlHelper;
            _logger = logger;
            _dao = dao;
        }
        public async Task<bool> AddAsync(ApplicationUserAddress entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "AddressLine1", Value = entity.AddressLine1},
                        new ParameterInfo {Name = "AddressLine2", Value = entity.AddressLine2},
                        new ParameterInfo {Name = "City", Value = entity.City},
                        new ParameterInfo {Name = "PostalCode", Value = entity.PostalCode},
                        new ParameterInfo {Name = "Country", Value = entity.Country},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.InsertSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to create ApplicationUserAddress record in Database");
                    throw;
                }
            }
            return result;
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            bool result = false;

            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo { Name = "Id", Value = Id }
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.DeleteSql(), _params, CommandType.Text) > 0;
                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to delete ApplicationUserAddress");
                    throw;
                }
            }
            return result;
        }

        public async Task<IReadOnlyList<ApplicationUserAddress>> GetAllAsync()
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo{ Name = "Id", Value = null }
                };
                return await _sqlHelper.GetRecordsAsync<ApplicationUserAddress>(_dao.GetAllSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retreive ApplicationUserAddress");
                throw;
            }
        }

        public async Task<ApplicationUserAddress> GetByIdAsync(int Id)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "Id", Value = Id }
                };
                return await _sqlHelper.GetRecordAsync<ApplicationUserAddress>(_dao.GetByIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve ApplicationUserAddress");
                throw;
            }
        }

        public async Task<ApplicationUserAddress> GetByUserIdAsync(int UserId)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "UserId", Value = UserId }
                };
                return await _sqlHelper.GetRecordAsync<ApplicationUserAddress>(_dao.GetByUserIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve ApplicationUserAddress");
                throw;
            }
        }

        public async Task<bool> UpdateAsync(ApplicationUserAddress entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "AddressLine1", Value = entity.AddressLine1},
                        new ParameterInfo {Name = "AddressLine2", Value = entity.AddressLine2},
                        new ParameterInfo {Name = "City", Value = entity.City},
                        new ParameterInfo {Name = "PostalCode", Value = entity.PostalCode},
                        new ParameterInfo {Name = "Country", Value = entity.Country},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.UpdateSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to update ApplicationUserAddress record in Database");
                    throw;
                }
            }
            return result;
        }
    }
}
