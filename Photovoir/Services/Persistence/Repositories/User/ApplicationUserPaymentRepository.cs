﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Photovoir.Data;
using Photovoir.Data.User;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.User;
using Photovoir.Interfaces.Repositories.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Photovoir.Services.Persistence.Repositories.User
{
    public class ApplicationUserPaymentRepository : IApplicationUserPaymentRepository
    {
        private readonly ISqlHelper _sqlHelper;
        private readonly ILogger<ApplicationUserPaymentRepository> _logger;
        private readonly IApplicationUserPaymentDao _dao;
        private readonly IConfiguration _config;

        public ApplicationUserPaymentRepository(IConfiguration configuration, IApplicationUserPaymentDao dao, ILogger<ApplicationUserPaymentRepository> logger, ISqlHelper sqlHelper)
        {
            _config = configuration;
            _sqlHelper = sqlHelper;
            _logger = logger;
            _dao = dao;
        }

        public async Task<bool> AddAsync(ApplicationUserPayment entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "CardNumber", Value = entity.CardNumber},
                        new ParameterInfo {Name = "NameOnCard", Value = entity.NameOnCard},
                        new ParameterInfo {Name = "Expiry", Value = entity.Expiry},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.InsertSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to create ApplicationUserPayment record in Database");
                    throw;
                }
            }
            return result;
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            bool result = false;

            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo { Name = "Id", Value = Id }
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.DeleteSql(), _params, CommandType.Text) > 0;
                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to delete ApplicationUserPayment");
                    throw;
                }
            }
            return result;
        }

        public async Task<IReadOnlyList<ApplicationUserPayment>> GetAllAsync()
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo{ Name = "Id", Value = null }
                };
                return await _sqlHelper.GetRecordsAsync<ApplicationUserPayment>(_dao.GetAllSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retreive ApplicationUserPayment");
                throw;
            }
        }

        public async Task<ApplicationUserPayment> GetByIdAsync(int Id)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "Id", Value = Id }
                };
                return await _sqlHelper.GetRecordAsync<ApplicationUserPayment>(_dao.GetByIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve ApplicationUserPayment");
                throw;
            }
        }

        public async Task<ApplicationUserPayment> GetByUserIdAsync(int UserId)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "UserId", Value = UserId }
                };
                return await _sqlHelper.GetRecordAsync<ApplicationUserPayment>(_dao.GetByUserIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve ApplicationUserAddress");
                throw;
            }
        }

        public async Task<bool> UpdateAsync(ApplicationUserPayment entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "CardNumber", Value = entity.CardNumber},
                        new ParameterInfo {Name = "NameOnCard", Value = entity.NameOnCard},
                        new ParameterInfo {Name = "Expiry", Value = entity.Expiry},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.UpdateSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to update ApplicationUserPayment record in Database");
                    throw;
                }
            }
            return result;
        }
    }
}
