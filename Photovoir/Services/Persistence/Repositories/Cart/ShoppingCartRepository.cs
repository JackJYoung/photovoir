﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Photovoir.Data;
using Photovoir.Data.Cart;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.Cart;
using Photovoir.Interfaces.Repositories.Cart;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Photovoir.Services.Persistence.Repositories.Cart
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly ISqlHelper _sqlHelper;
        private readonly ILogger<ShoppingCartRepository> _logger;
        private readonly IShoppingCartDao _dao;
        private readonly IConfiguration _config;

        public ShoppingCartRepository(IConfiguration configuration, IShoppingCartDao dao, ILogger<ShoppingCartRepository> logger, ISqlHelper sqlHelper)
        {
            _config = configuration;
            _sqlHelper = sqlHelper;
            _logger = logger;
            _dao = dao;
        }
        public async Task<bool> AddAsync(ShoppingCart entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "Total", Value = entity.Total},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.InsertSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to create ShoppingCart record in Database");
                    throw;
                }
            }
            return result;
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            bool result = false;

            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo { Name = "Id", Value = Id }
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.DeleteSql(), _params, CommandType.Text) > 0;
                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to delete ShoppingCart");
                    throw;
                }
            }
            return result;
        }

        public async Task<IReadOnlyList<ShoppingCart>> GetAllAsync()
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo{ Name = "Id", Value = null }
                };
                return await _sqlHelper.GetRecordsAsync<ShoppingCart>(_dao.GetAllSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retreive ShoppingCart");
                throw;
            }
        }

        public async Task<ShoppingCart> GetByIdAsync(int Id)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "Id", Value = Id }
                };
                return await _sqlHelper.GetRecordAsync<ShoppingCart>(_dao.GetByIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve ShoppingCart");
                throw;
            }
        }

        public async Task<bool> UpdateAsync(ShoppingCart entity)
        {
            if (entity == null)
                throw new ArgumentNullException("ShoppingCart data cannot be null");

            bool result;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "Total", Value = entity.Total},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.UpdateSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to update ShoppingCart Data");
                    throw;
                }
            }
            return result;
        }
    }
}
