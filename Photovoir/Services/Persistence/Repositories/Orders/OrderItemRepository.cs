﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Photovoir.Data;
using Photovoir.Data.Orders;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.Order;
using Photovoir.Interfaces.Repositories.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Photovoir.Services.Persistence.Repositories.Orders
{
    public class OrderItemRepository : IOrderItemRepository
    {
        private readonly ISqlHelper _sqlHelper;
        private readonly ILogger<OrderItemRepository> _logger;
        private readonly IOrderItemDao _dao;
        private readonly IConfiguration _config;

        public OrderItemRepository(IConfiguration configuration, IOrderItemDao dao, ILogger<OrderItemRepository> logger, ISqlHelper sqlHelper)
        {
            _config = configuration;
            _sqlHelper = sqlHelper;
            _logger = logger;
            _dao = dao;
        }

        public async Task<bool> AddAsync(OrderItem entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "OrderId", Value = entity.OrderId},
                        new ParameterInfo {Name = "ProductId", Value = entity.ProductId},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.InsertSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to create CartItem record in Database");
                    throw;
                }
            }
            return result;
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            bool result = false;

            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo { Name = "Id", Value = Id }
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.DeleteSql(), _params, CommandType.Text) > 0;
                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to delete CartItem");
                    throw;
                }
            }
            return result;
        }

        public async Task<IReadOnlyList<OrderItem>> GetAllAsync()
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo{ Name = "Id", Value = null }
                };
                return await _sqlHelper.GetRecordsAsync<OrderItem>(_dao.GetAllSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retreive CartItem");
                throw;
            }
        }

        public async Task<OrderItem> GetByIdAsync(int Id)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "Id", Value = Id }
                };
                return await _sqlHelper.GetRecordAsync<OrderItem>(_dao.GetByIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve CartItem");
                throw;
            }
        }

        public async Task<bool> UpdateAsync(OrderItem entity)
        {
            if (entity == null)
                throw new ArgumentNullException("CartItem data cannot be null");

            bool result;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "OrderId", Value = entity.OrderId},
                        new ParameterInfo {Name = "ProductId", Value = entity.ProductId},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.UpdateSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to update CartItem Data");
                    throw;
                }
            }
            return result;
        }
    }
}
