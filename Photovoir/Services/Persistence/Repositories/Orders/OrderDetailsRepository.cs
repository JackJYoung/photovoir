﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Photovoir.Data;
using Photovoir.Data.Orders;
using Photovoir.Interfaces;
using Photovoir.Interfaces.Dao.Order;
using Photovoir.Interfaces.Repositories.Order;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Photovoir.Services.Persistence.Repositories.Orders
{
    public class OrderDetailsRepository : IOrderDetailsRepository
    {
        private readonly ISqlHelper _sqlHelper;
        private readonly ILogger<OrderDetailsRepository> _logger;
        private readonly IOrderDetailsDao _dao;
        private readonly IConfiguration _config;

        public OrderDetailsRepository(IConfiguration configuration, IOrderDetailsDao dao, ILogger<OrderDetailsRepository> logger, ISqlHelper sqlHelper)
        {
            _config = configuration;
            _sqlHelper = sqlHelper;
            _logger = logger;
            _dao = dao;
        }
        public async Task<bool> AddAsync(OrderDetails entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            bool result = false;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "PaymentId", Value = entity.PaymentId},
                        new ParameterInfo {Name = "Total", Value = entity.Total},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.InsertSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to create OrderDetails record in Database");
                    throw;
                }
            }
            return result;
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            bool result = false;

            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo { Name = "Id", Value = Id }
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.DeleteSql(), _params, CommandType.Text) > 0;
                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to delete OrderDetails");
                    throw;
                }
            }
            return result;
        }

        public async Task<IReadOnlyList<OrderDetails>> GetAllAsync()
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo{ Name = "Id", Value = null }
                };
                return await _sqlHelper.GetRecordsAsync<OrderDetails>(_dao.GetAllSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retreive OrderDetails");
                throw;
            }
        }

        public async Task<OrderDetails> GetByIdAsync(int Id)
        {
            try
            {
                List<ParameterInfo> _params = new List<ParameterInfo>
                {
                    new ParameterInfo { Name = "Id", Value = Id }
                };
                return await _sqlHelper.GetRecordAsync<OrderDetails>(_dao.GetByIdSql(), _params, CommandType.Text);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to retrieve OrderDetails");
                throw;
            }
        }

        public async Task<bool> UpdateAsync(OrderDetails entity)
        {
            if (entity == null)
                throw new ArgumentNullException("OrderDetails data cannot be null");

            bool result;
            using (Transaction trans = new Transaction(_config))
            {
                try
                {
                    List<ParameterInfo> _params = new List<ParameterInfo>
                    {
                        new ParameterInfo {Name = "UserId", Value = entity.UserId},
                        new ParameterInfo {Name = "PaymentId", Value = entity.PaymentId},
                        new ParameterInfo {Name = "Total", Value = entity.Total},
                    };
                    result = await _sqlHelper.ExecuteQueryAsync(trans.GetConnection(), trans.GetTransaction(), _dao.UpdateSql(), _params, CommandType.Text) > 0;

                    if (result)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    _logger.LogError(ex, "Failed to update OrderDetails Data");
                    throw;
                }
            }
            return result;
        }
    }
}
